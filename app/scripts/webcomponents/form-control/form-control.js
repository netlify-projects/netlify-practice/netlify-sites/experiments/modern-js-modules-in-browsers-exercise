
class FormControl extends HTMLElement {
  static get observedAttributes() {
    return [ 'input-type', 'input-name', 'input-label', 'input-placeholder'];
  }

  constructor() {
    super();

    const shadowRoot = this.attachShadow({mode: 'open'});
  }

  createInputElementOfType(inputType) {
    const createdInput = document.createElement('input');
    createdInput.setAttribute('type', inputType);
    return createdInput;
  }

  createLabelElementForInput(targetInputName) {
    const labelElement = document.createElement('label');
    labelElement.setAttribute('for', `${targetInputName}`);
    // labelElement.innerHTML = targetInputName.capitalize();
    // labelElement.innerHTML = targetInputName;
    labelElement.innerHTML = 'Blah';
    return labelElement;
  }

  configureInputElement(inputElement) {
    inputElement.setAttribute('name', `${this.inputName}`);
    inputElement.setAttribute('id', `${this.inputName}`);
    inputElement.setAttribute('placeholder', this.inputPlaceholder);
  }

  styleElements() {
    const styleEl = document.createElement('style');
    styleEl.innerText = `
    label, input {
      display: block;
      width: 100%;
    }
    `;
    return styleEl;
  }

  render() {
    const wrapperElement = document.createElement('div');
    const inputEl = this.createInputElementOfType(this.inputType);
    const labelEl = this.createLabelElementForInput(this.inputName);
    const styleElement = this.styleElements();
    this.shadowRoot.appendChild(styleElement);
    this.configureInputElement(inputEl);
    wrapperElement.appendChild(labelEl);
    wrapperElement.appendChild(inputEl);
    this.shadowRoot.appendChild(wrapperElement);
  }

  connectedCallback() {
    this.render();
  }

  get inputType() {
    return +this.getAttribute('input-type');
  }
  get inputName() {
    return +this.getAttribute('input-name');
  }
  get inputLabel() {
    return +this.getAttribute('input-label');
  }
  get inputPlaceholder() {
    return +this.getAttribute('input-placeholder');
  }

  disconnectedCallback() {

  }

  attributeChangedCallback(name, oldVal, newVal) {
    if (oldVal !== newVal) {
      console.log(`${name} changed from ${oldVal} to ${newVal}`)
      switch(name) {
        case 'input-label':
          this.inputLabel = newVal;
          break;
        case 'input-name':
          this.inputName = newVal;
          break;
        case 'input-placeholder':
          this.inputPlaceholder = newVal;
          break;
        case 'input-type':
          this.inputType = newVal;
          break;
      }
      this.render();
    }
  }
}

customElements.define('form-control', FormControl);

// export { FormControl };
module.exports = { FormControl };
